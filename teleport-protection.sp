#define PLUGIN_AUTHOR ".#Zipcore"
#define PLUGIN_NAME "Teleport Protection"
#define PLUGIN_VERSION "1.1"
#define PLUGIN_DESCRIPTION ""
#define PLUGIN_URL "zipcore.net"

#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <cstrike>
#include <csgocolors>
#include <smlib>

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

enum FX
{
	FxNone = 0,
	FxPulseFast,
	FxPulseSlowWide,
	FxPulseFastWide,
	FxFadeSlow,
	FxFadeFast,
	FxSolidSlow,
	FxSolidFast,
	FxStrobeSlow,
	FxStrobeFast,
	FxStrobeFaster,
	FxFlickerSlow,
	FxFlickerFast,
	FxNoDissipation,
	FxDistort,               // Distort/scale/translate flicker
	FxHologram,              // kRenderFxDistort + distance fade
	FxExplode,               // Scale up really big!
	FxGlowShell,             // Glowing Shell
	FxClampMinScale,         // Keep this sprite from getting very small (SPRITES only!)
	FxEnvRain,               // for environmental rendermode, make rain
	FxEnvSnow,               //  "        "            "    , make snow
	FxSpotlight,     
	FxRagdoll,
	FxPulseFastWider,
};

enum Render
{
	Normal = 0,     // src
	TransColor,     // c*a+dest*(1-a)
	TransTexture,    // src*a+dest*(1-a)
	Glow,        // src*a+dest -- No Z buffer checks -- Fixed size in screen space
	TransAlpha,      // src*srca+dest*(1-srca)
	TransAdd,      // src*a+dest
	Environmental,    // not drawn, used for environmental effects
	TransAddFrameBlend,  // use a fractional frame value to blend between animation frames
	TransAlphaAdd,    // src + dest*(1-a)
	WorldGlow,      // Same as kRenderGlow but not fixed size in screen space
	None,        // Don't render.
};

int g_iOffRender = -1;

ConVar cvProtectionTime;
float g_fProtectionTime;

ConVar cvRed;
int g_iRed;

ConVar cvGreen;
int g_iGreen;

ConVar cvBlue;
int g_iBlue;

ConVar cvAlpha;
int g_iAlpha;

ConVar cvShoot;
bool g_bShoot;

ConVar cvSpeedAdvantage;
float g_fSpeedAdvantage;

ConVar cvSpeedDisadvantage;
float g_fSpeedDisadvantage;

float g_fLastTeleport[MAXPLAYERS + 1];
float g_fLastSpeed[MAXPLAYERS + 1];

bool g_bColored[MAXPLAYERS + 1];
bool g_bSpeed[MAXPLAYERS + 1];

ConVar cvMaxvelocity;
float g_fMaxvelocity;

float g_fPos1[MAXPLAYERS+1][3];
float g_fPos2[MAXPLAYERS+1][3];

public Plugin myinfo = 
{
	name = PLUGIN_NAME,
	author = PLUGIN_AUTHOR,
	description = PLUGIN_DESCRIPTION,
	version = PLUGIN_VERSION,
	url = PLUGIN_URL
}

public void OnPluginStart()
{
	CreateConVar("telep_version", PLUGIN_VERSION, "Teleport Protection version", FCVAR_DONTRECORD|FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	
	cvProtectionTime = CreateConVar("telep_protection_time", "1.0", "Amount of time to protect the player.");
	g_fProtectionTime = GetConVarFloat(cvProtectionTime);
	HookConVarChange(cvProtectionTime, OnSettingChanged);
	
	cvRed = CreateConVar("telep_color_red", "255", "Red");
	g_iRed = GetConVarInt(cvRed);
	HookConVarChange(cvRed, OnSettingChanged);
	
	cvGreen = CreateConVar("telep_color_green", "236", "Green");
	g_iGreen = GetConVarInt(cvGreen);
	HookConVarChange(cvGreen, OnSettingChanged);
	
	cvBlue = CreateConVar("telep_color_blue", "55", "Blue");
	g_iBlue = GetConVarInt(cvBlue);
	HookConVarChange(cvBlue, OnSettingChanged);
	
	cvAlpha = CreateConVar("telep_color_alpha", "120", "Change player alpha while protected");
	g_iAlpha = GetConVarInt(cvAlpha);
	HookConVarChange(cvAlpha, OnSettingChanged);
	
	cvShoot = CreateConVar("telep_shoot", "1", "Allow players to shoot while protected.");
	g_bShoot = GetConVarBool(cvShoot);
	HookConVarChange(cvShoot, OnSettingChanged);
	
	cvSpeedAdvantage = CreateConVar("telep_speed_advantage", "1800.0", "If you enter a portal faster than this you can break the protection of players which have been teleported before you. (-1.0: Disable)");
	g_fSpeedAdvantage  = GetConVarFloat(cvSpeedAdvantage);
	HookConVarChange(cvSpeedAdvantage, OnSettingChanged);
	
	cvSpeedDisadvantage = CreateConVar("telep_speed_disadvantage", "1800.0", "If you enter a portal faster than this you won't get teleport protection. (-1.0: Disable)");
	g_fSpeedDisadvantage  = GetConVarFloat(cvSpeedDisadvantage);
	HookConVarChange(cvSpeedDisadvantage, OnSettingChanged);
	
	cvMaxvelocity = FindConVar("sv_maxvelocity");
	g_fMaxvelocity  = GetConVarFloat(cvMaxvelocity);
	HookConVarChange(cvMaxvelocity, OnSettingChanged);
	
	AutoExecConfig(true, "teleport-protection");
	
	g_iOffRender = FindSendPropInfo("CBasePlayer", "m_clrRender");
	
	HookEvent("player_spawn", Event_Spawn);
	HookEvent("weapon_fire", Event_WeaponFire);
	
	HookEntityOutput("trigger_teleport", "OnEndTouch", OnTeleport);
	
	LoopIngameClients(i)
		OnClientPutInServer(i);
}

public int OnSettingChanged(Handle convar, const char[] oldValue, const char[] newValue)
{
	if (convar == cvProtectionTime)
		g_fProtectionTime = StringToFloat(newValue);
	else if (convar == cvRed)
		g_iRed = StringToInt(newValue);
	else if (convar == cvGreen)
		g_iGreen = StringToInt(newValue);
	else if (convar == cvBlue)
		g_iBlue = StringToInt(newValue);
	else if (convar == cvAlpha)
		g_iAlpha = StringToInt(newValue);
	else if (convar == cvShoot)
		g_bShoot = view_as<bool>(StringToInt(newValue));
	else if (convar == cvSpeedAdvantage)
		g_fSpeedAdvantage = StringToFloat(newValue);
	else if (convar == cvSpeedDisadvantage)
		g_fSpeedDisadvantage = StringToFloat(newValue);
	else if (convar == cvMaxvelocity)
		g_fMaxvelocity = StringToFloat(newValue);
}

public void OnMapStart()
{
	HookEntityOutput("trigger_teleport", "OnEndTouch", OnTeleport);
}

public void OnClientPutInServer(int client)
{
	SDKHook(client, SDKHook_OnTakeDamage, OnTakeDamage);
}

public void OnClientDisconnect(int client)
{
	g_fLastTeleport[client] = 0.0;
	g_bColored[client] = false;
	g_bSpeed[client] = false;
}

public Action Event_Spawn(Handle event, const char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	g_fLastTeleport[client] = 0.0;
	g_bColored[client] = false;
	g_bSpeed[client] = false;
	FX_Render(client);
	
	return Plugin_Continue;
}

public int OnTeleport(const char[] output, int caller, int iClient, float data)
{
	if(iClient == 0)
		return;
	
	if(!Entity_IsPlayer(iClient))
		return;
	
	if(!Client_Moved2Fast(iClient, true))
		return;
	
	g_fLastTeleport[iClient] = GetGameTime();
	g_fLastSpeed[iClient] = GetClientSpeed(iClient);
	
	if(g_fSpeedDisadvantage == -1.0 || g_fLastSpeed[iClient] < g_fSpeedDisadvantage)
	{
		CPrintToChat(iClient, "{green}Protection activated.");
		
		g_bColored[iClient] = true;
		FX_Render(iClient, view_as<FX>(FxDistort), g_iRed, g_iGreen, g_iBlue, view_as<Render>(RENDER_TRANSADD), g_iAlpha);
	}
	else if(g_bColored[iClient])
	{
		g_bColored[iClient] = false;
		FX_Render(iClient);
		CPrintToChat(iClient, "{darkred}Protection deactivated.");
	}
	
	if(g_fSpeedAdvantage != -1.0 && g_fLastSpeed[iClient] > g_fSpeedAdvantage)
	{
		CPrintToChat(iClient, "{yellow}Speed advantage {green}activated.");
	}
	else if(g_bSpeed[iClient])
	{
		g_bSpeed[iClient] = false;
		CPrintToChat(iClient, "{yellow}Speed advantage {darkred}deactivated.");
	}
}

float GetClientSpeed(int iClient)
{
	float fVelocity[3];
	GetEntPropVector(iClient, Prop_Data, "m_vecVelocity", fVelocity);
	return SquareRoot(Pow(fVelocity[0], 2.0) + Pow(fVelocity[1], 2.0));
}

public Action OnPlayerRunCmd(int iClient, int &buttons, int &impulse, float vel[3], float angles[3], int &weapon, int &subtype, int &cmdnum, int &tickcount, int &seed, int mouse[2])
{
	if(!IsPlayerAlive(iClient))
		return Plugin_Continue;
	
	if(g_bSpeed[iClient] && HasSpeedAdvantage(iClient))
	{
		g_bSpeed[iClient] = false;
		CPrintToChat(iClient, "{yellow}Speed advantage {darkred}deactivated.");
	}
	
	if(!HasProtection(iClient))
	{
		if(g_bColored[iClient])
		{
			g_bColored[iClient] = false;
			FX_Render(iClient);
			CPrintToChat(iClient, "{darkred}Protection deactivated.");
		}
		return Plugin_Continue;
	}
	
	if(!g_bShoot)
		BlockWeapon(iClient, weapon, GetProtectionTime(iClient));
		
	return Plugin_Continue;
}

public Action Event_WeaponFire(Handle event, const char[] name, bool dontBroadcast)
{
	int iClient = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if(!HasProtection(iClient))
		return Plugin_Continue;
	
	g_fLastTeleport[iClient] = 0.0;
	g_bColored[iClient] = false;
	FX_Render(iClient);
	CPrintToChat(iClient, "{darkred}Protection deactivated.");
	
	return Plugin_Continue;
}

public Action OnTakeDamage(int victim, int &attacker, int &inflictor, float &damage, int &damagetype, int &weapon, float damageForce[3], float damagePosition[3])
{
	if(attacker == 0)
		return Plugin_Continue;
		
	if(!Entity_IsPlayer(attacker))
		return Plugin_Continue;
	
	if(HasProtection(victim))
	{
		if(g_fSpeedAdvantage != -1.0 && g_fLastSpeed[attacker] > g_fSpeedAdvantage)
		{
			CPrintToChatAll("{purple}%N {green}used {yellow}speed advantage {green}to break {purple}%N's {green}protection.", attacker, victim);
			return Plugin_Continue;
		}
		
		CPrintToChat(attacker, "{purple}%N {green}is protected.", victim);
		return Plugin_Handled;
	}
	
	return Plugin_Continue;
}

bool HasProtection(int client)
{
	if(g_fSpeedDisadvantage != -1.0 && g_fLastSpeed[client] > g_fSpeedDisadvantage)
		return false;
	
	return GetGameTime() - g_fProtectionTime < g_fLastTeleport[client];
}

bool HasSpeedAdvantage(int client)
{
	if(g_fSpeedAdvantage == -1.0)
		return false;
	
	if(g_fLastSpeed[client] < g_fSpeedAdvantage)
		return false;
	
	return GetGameTime() - g_fProtectionTime < g_fLastTeleport[client];
}

float GetProtectionTime(int client)
{
	float fTime = g_fLastTeleport[client] + g_fProtectionTime - GetGameTime();
	
	return fTime > 0.0 ? fTime : 0.0;
}

void BlockWeapon(int client, int weapon, float time)
{
	float fUnlockTime = GetGameTime() + time;
	
	SetEntPropFloat(client, Prop_Send, "m_flNextAttack", fUnlockTime);
	
	if(weapon > 0)
		SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", fUnlockTime);
}

stock void FX_Render(int client, FX fx = FxNone, int r = 255, int g = 255, int b = 255, Render render = Normal, int alpha = 255)
{
	SetEntProp(client, Prop_Send, "m_nRenderFX", fx, 1);
	SetEntProp(client, Prop_Send, "m_nRenderMode", render, 1);  
	SetEntData(client, g_iOffRender, r, 1, true);
	SetEntData(client, g_iOffRender + 1, g, 1, true);
	SetEntData(client, g_iOffRender + 2, b, 1, true);
	SetEntData(client, g_iOffRender + 3, alpha, 1, true);  
}

public void OnGameFrame()
{
	LoopAlivePlayers(iClient)
	{
		g_fPos1[iClient] = g_fPos2[iClient];
		GetClientAbsOrigin(iClient, g_fPos2[iClient]);
	}
}

bool Client_Moved2Fast(int iClient, bool update)
{
	if(update)
		GetClientAbsOrigin(iClient, g_fPos2[iClient]);
	return GetVectorDistance(g_fPos1[iClient], g_fPos2[iClient]) > (g_fMaxvelocity / 50.0);
}